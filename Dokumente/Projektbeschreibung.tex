\documentclass[11pt, a4paper]{scrartcl}
\usepackage[german]{babel}
\usepackage[utf8]{inputenc}
\usepackage[headsepline=0.7pt]{scrlayer-scrpage}
\usepackage[onehalfspacing]{setspace}
\renewcommand{\rmdefault}{phv}
\usepackage[urlcolor=blue]{hyperref}

\title{Das \glqq Religioracle\grqq\\Projektbeschreibung}
\subtitle{Projekt im Rahmen des Moduls "Information Retrieval" 2019\\bei Jun.-Prof. Dr. Martin Potthast und Shahbaz Syed}
\author{Jonas Lürig\\Till Mahlburg\\Jenny Pretzsch\\Malte Tammena\\Tom Winter}
\date{Version vom \today}

\parskip1mm
\parindent0mm

\pagestyle{scrheadings}
\clearpairofpagestyles
\setkomafont{pagehead}{\footnotesize}
\chead{\normalfont Das "Religioracle", \today, J. Lürig, T. Mahlburg, J. Pretzsch, M. Tammena, T. Winter}
\cfoot{\pagemark}


\begin{document}
\maketitle
\newpage
\tableofcontents
\newpage


\section{Einleitung und Motivation}
\begin{center}

\begin{large}
\textbf{\glqq Jerusalem: Unruhen am Tag des muslimischen Opferfestes\grqq}\newline
\end{large}
(Süddeutsche Zeitung, 11.08.2019, 12:36 Uhr)\footnote{https://www.sueddeutsche.de/panorama/jerusalem-unruhen-am-tag-des-muslimischen-opferfestes-1.4560405}

\begin{large}
\textbf{\glqq Erneut Angriff auf jüdischen Mann in Berlin\grqq}\newline
\end{large}
(Zeit Online, 14.08.2019, 12:16 Uhr)\footnote{https://www.zeit.de/gesellschaft/2019-08/antisemitismus-berlin-angriff-jude-charlottenburg}

\begin{large}
\textbf{\glqq Norwegen: Verletzte nach Schüssen in Moschee\grqq}\newline
\end{large}
(Zeit Online, 10.08.2019, 20:16 Uhr)\footnote{https://www.zeit.de/gesellschaft/2019-08/norwegen-moschee-schuesse-ein-verletzter-oslo}

\end{center}


Unsere heutige Zeit ist geprägt von Konflikten. Einen nicht unwesentlichen Teil dieser Konflikte nehmen Spannungen zwischen Religionen ein. Speziell die großen monotheistischen Religionen -- der Islam, das Judentum und das Christentum -- stehen häufig im Mittelpunkt der Schlagzeilen.

Eine viel diskutierte Frage bei der Erklärung dieser Konflikte lautet: Was unterscheidet diese Religionen? Wir möchten einen Schritt weiter gehen und außerdem fragen: Was haben diese Religionen gemeinsam?

Im Rahmen des Moduls \glqq Information Retrieval\grqq{} hat es sich unsere Projektgruppe zur Aufgabe gemacht, eine Suchmaschine für die religiösen Schriften der drei großen monotheistischen Religionen zu entwickeln.

Nutzer können nach Stichworten oder zusammenhängenden Phrasen suchen, die in der Torah, dem Koran und der Bibel gefunden werden können.

Ziel ist es, eine bessere Vergleichbarkeit der Religionen zu erreichen. Um gefundene Texte geschichtlich einordnen zu können, haben wir zusätzlich die Jahreszahlen, zu denen die Texte wahrscheinlich verfasst wurden, in die Suchmaschine eingearbeitet. Die Antwort auf eine spezielle Frage könnte also in der Vergangenheit von Jahrhundert zu Jahrhundert unterschiedlich beantwortet worden sein.


\section{Die religiösen Schriften}

\subsection{Quelle}

Die Daten der religiösen Schriften haben wir in deutscher Sprache aus dem Web bezogen. Die Bibel ist als zip-Paket bereitgestellt, während die anderen beiden Werke als Sammlung von HTML-Seiten verarbeitet werden können\footnote{Bibel: https://bibel.github.io}\footnote{Koran: https://koran.rocks}\footnote{Torah: https://www.talmud.de/tlmd/die-torah-eine-deutsche-uebersetzung/die-torah-bereschit/}. Diese lagern im Container des Projekts, von wo sie ein Parser aufruft, die erforderlichen Texte herausfiltert und in JSON-Objekte umwandelt.


\subsection{Aufbau der Schriften}\label{sec:Aufbau der Schriften}

Die Schriften der Bibel sind gemäß ihres historischen Aufbaus in zwei Ordner unterteilt: Altes und Neues Testament. Jedoch ist jedes Kapitel eines biblischen Buches als einzelnes HTML-Dokument abgespeichert. Im Alten Testament beläuft sich die Summe der zu parsenden Dokumente somit auf 1.068 Dateien, im kürzeren Neuen Testament sind es 260 Dateien. Die Dateinamen setzen sich aus dem Titel des Buches und der Kapitelnummer zusammen.

Der Koran bietet mit 114 Suren weniger Textstoff, dafür sind die einzelnen Suren mitunter sehr lang (teilweise über 38.000 Zeichen). In den Namen der Quelldateien ist stets auch die Surennummer vertextlicht enthalten, während der Name im Anschluss an Bindestriche angehängt wird.

Die Torah liegt als eine Menge von 55 Ordnern vor, wobei jeder Ordner eine index.html-Datei beinhaltet. Diese Dateien sind gemäß der jüdischen Aufteilung der fünf Bücher Moses, welche die Torah bilden, separiert. Jedes Buch besteht aus etwa 10 bis 12 Teilen, wobei jeder Teil mehrere Kapitel umfassen kann. Die Namen der jeweiligen Teile befinden sich im Titel des Ordners.


\subsection{Zeitliche Einordnung einzelner Teile}

Um die zeitliche Einordnung der Erstellung der Texte einzuarbeiten, haben wir unsere Recherchen zum Alter des jeweiligen Textausschnitts in einer JSON-Datei festgehalten. Für jeden Text findet sich ein Anfangs- und ein Endjahr, welche genau die Zeitspanne umfassen, in der die Schrift wahrscheinlich geschrieben wurde. Dabei können die Angaben von einem exakten Jahr bis zu einer Spanne von mehr als drei Jahrhunderten variieren -- je nach aktuellem Forschungsstand.\newline
Auffällig ist, dass beispielsweise alle Texte des Korans vom Propheten Mohammed in wenigen Jahren (unterteilt in zwei Perioden) geschrieben wurden, während die Texte, die in der Bibel vorkommen (insbesondere zwischen Altem und Neuem Testament) teilweise viele Jahrhunderte trennen.


\section{Verwendete Tools und Frameworks}

\subsection{Docker}

Als Grundlage der Software werden zwei Docker-Container verwendet. Im ersten ist Solr isoliert installiert, damit die Suchmaschine stets problemlos Anfragen verarbeiten kann. Da der Parser, der auch in diesem Container läuft, in Python 3 geschrieben ist, ist zusätzlich noch eine aktuelle Version von Python installiert.

Im zweiten Docker-Container liegt das gesamte Frontend. Hier sind also die Webseiten abgelegt, genauso wie die Django-Files, die die Funktionalität der Software und die Datenbank-Anbindung gewährleisten.


\subsection{Suchmaschinen-Backend}

Als Suchmaschinen-Backend wird Apache Solr verwendet. Die Anfragen, die wir an Solr stellen, werden zunächst aufbereitet, um beispielsweise Phrasen zu erkennen und weiterzugeben oder um die Gewichtung der einzelnen Anfrage-Teile sowie geeignete Editier-Distanzen entsprechend unserer Vorstellungen einzustellen.


\subsection{Web-Frontend}

\subsubsection{Django}

Django ist ein Webframework für die Programmiersprache Python. In erster Linie nutzen wir es, um unsere Webseite zu erstellen. Außerdem verwenden wir die Einrichtungen und die Datenbank des Frameworks, um unsere Evaluation durchzuführen und die Ergebnisse mit ihrem Rating zu speichern. Darüber hinaus passiert hier die bereits erwähnte Aufbereitung der Anfragen.


\subsubsection{Spectre}

Um uns die Arbeit mit der Gestaltung der Webseite einfacher zu machen, nutzen wir das Spectre.css-Framework. Dadurch entspricht unsere Seite bspw. bereits weitgehend den Vorgaben eines responsiven Designs.


\subsubsection{SCSS}

Die Stylesheetsprache SCSS nutzen wir, um Unzulänglichkeiten von  CSS zu umgehen. Besipielsweise ermöglicht SCSS die Nutzung von Variablen und Schleifen. Über ein Plugin für Django werden die SCSS-Dateien in CSS übersetzt und funktionieren daher auf allen verbreiteten Browsern.


\subsection{Parser}

Der Parser wurde von unserer Gruppe mit Hilfe der Python-Bibliothek \glqq beautifulsoup4\grqq\newline
geschrieben, welche bei der Datenextraktion aus HTML-Dateien hilft. Nach Abschluss des Vorgangs liegen die Dokumente im JSON-Format vor, wobei pro HTML-Quelldatei genau eine JSON-Datei erstellt wird.


\section{Funktionsweise}

\subsection{Parser}

Die Quelldokumente, die in unterschiedlichen HTML-Formaten vorliegen, müssen zunächst in ein einheitliches Datenformat umgewandelt werden, welches problemlos von der Suchmaschine verarbeitet werden kann. Hier kommt der Parser zum Einsatz, der alle Quelldateien durchgeht und die gewünschten Inhalte filtert und als JSON speichert. Jede HTML-Quelldatei wird in ein JSON-File mit einem Objekt umgewandelt.

Bei der Filterung muss einerseits beachtet werden, dass es irrelevante Dokumente gibt (Einleitung, Vorwort, etc.), die nicht mitverarbeitet werden sollen und andererseits, dass jede religiöse Schrift in unterschiedlichem Format vorliegt (siehe \autoref{sec:Aufbau der Schriften} \glqq \nameref{sec:Aufbau der Schriften}\grqq).

Ist der relevante Inhalt eines Dokuments schließlich verarbeitet worden, wird dem Objekt auch noch die Zeitspanne in Form von zwei Jahreszahlen (Anfangs- und Endjahr) zugewiesen, welche das zeitliche Ranking ermöglicht. Die Daten dazu befinden sich in einer JSON-Datei, in der wir unsere Recherchen zum Alter der jeweiligen Teile formatiert abgelegt haben.


\subsection{Ranking}

Für unsere Darstellung von Suchergebnissen benötigen wir zwei unterschiedliche Rankings. Das eine Ranking soll die Relevanz eines Ergebnisses für die User-Anfrage darstellen, das andere die zeitliche Einordnung.

Das Relevanz-Ranking berechnet sich aus dem direkten Feedback, das wir im Rahmen der Evaluation den Query-Resultaten zugewiesen haben. Das Ranking, welches für die Benutzung der Suchmaschine verwendet wird, basiert auf der aktuellsten Iteration.

Das Ranking nach Alter wird strikt numerisch aus den Daten ermittelt, die den Textteilen beim Umwandeln ins JSON-Format zugeordnet wurden. Als Wert eines Dokuments wird hier der Mittelpunkt der infrage kommenden Zeitspanne verwendet.


\subsection{Darstellung}

Zu einer Anfrage werden maximal 30 Ergebnisse angezeigt. Hierbei wird stets der gesamte Textteil unabhängig von seiner Länge ausgegeben. In der Bibel entspricht das beispielsweise einem Kapitel, im Koran einer Sure.

Wird der Darstellungs-Regler auf \glqq chronologisch\grqq{} gestellt, dann entspricht die Reihenfolge der angezeigten Suchergebnisse der Sortierung nach Alter der Texte (wobei der älteste Text oben steht). Die Relevanz eines jeden Ergebnisses drückt sich durch die Codierung als Graustufe aus: Je heller ein Text dargestellt wird, desto relevanter ist er für die Anfrage.

Steht der Darstellungs-Regler hingegen nicht auf \glqq chronologisch\grqq, werden die Texte, wie bei Suchmaschinen üblich, gemäß ihres Relevanz-Scores absteigend sortiert.


\subsection{Export und Import}

\subsubsection{Export}\label{sec:Export}

Um die Daten, die sich bereits in der Datenbank befinden (wie z.B. Evaluations-Ratings) zu sichern, ist in die Weboberfläche ein Export-Mechanismus eingebaut. Bei Aufruf der URL-Ergänzung \glqq /tool/export\_db\grqq{} werden alle Daten der Django-Datenbank (außer die stets neu berechneten Scores) im JSON-Format exportiert und gespeichert. Vorher schon an gleicher Stelle gesicherte Daten werden durch den neuen Datenbank-Dump überschrieben. Weitere Informationen finden sich in der \glqq README.md\grqq.


\subsubsection{Import}

Beim Start von Django werden die Import-Funktionen der einzelnen Django-Models aufgerufen (Evaluations-Queries, Iterationen, Resultate, Query-Iterations-Paare und Ratings). Der Import eines Models wird jedoch nur aufgerufen, wenn die Anzahl der Instanzen zum Zeitpunkt des Starts gleich 0 ist (für Evaluations-Queries, Resultate und Ratings) oder wenn die Anzahl der zu importierenden Objekte größer ist als die gegenwärtig gespeicherten (für Iterationen und Query-Iterations-Paare).\newline
Nachdem der Import abgeschlossen ist, werden alle Scores neu berechnet.


\section{Evaluation}

Für die Evaluation wurden 50 Use-Cases erstellt, die realistische Nutzer-Anfragen darstellen. Diese wurden in einer JSON-Datei gespeichert und pro Iteration in Form von Queries ausgeführt. Dabei sind speziell auch mögliche User-Fehler wie z.B. Tippfehler in den Queries vorhanden.

Insgesamt wurden für die Evaluation sechs Iterationen durchgeführt. Dabei wurde jeweils manuell durch direktes Feedback die Präzision der gefundenen Ergebnisse übermittelt und ausgewertet. Eine genaue Übersicht der Iterationen bietet die folgende Tabelle:

\begin{tabular}{|p{5mm}|p{2.5cm}|p{1cm}|p{1.5cm}|p{1cm}|p{1.3cm}|p{5cm}|}
\hline
Nr. & Iteration & \multicolumn{2}{|c|}{Chron. Score} & \multicolumn{2}{|c|}{Relev. Score} & Changes \\ \hline
"" & "" & abs. & rel. & abs. & rel. & "" \\ \hline
1 & baseline & 0.328 & -- & 0.171 & -- & executed all queries in the projects basic configuration \\ \hline
2 & expanded search to title fields & 0.355 & +8.0\% & 0.197 & +15.4\% & Title fields are now searched as well and have double the weight of text fields \\ \hline
3 & implicit phrase search & 0.228 & -35.8\% & 0.150 & +23.8\% & try to use the users input in a phrase search with double the weight if it wasn't one already \\ \hline
4 & fuzzy search & 0.314 & +37.8\% & 0.179 & +18.9\% & allows errors in words if the query isn't a phrase search \\ \hline
5 & improved query splitting and weighting changes & 0.331 & +5.3\% & 0.187 & +4.6\% & Query splitting now differentiates between words and phrases. Also changed the weights to be more in line with expected importance of query parts \\ \hline
\hline
\end{tabular}

\bigskip
Der absolute Score einer Iteration wird aus dem Durchschnitt der Scores aller Queries gebildet. Der relative Score bezieht sich immer auf das Verhältnis zur vorherigen Iteration.


\subsection{Speicherort der Ratings}

Im Unterordner \glqq /ir\_project/db\_export\_json/ratings\grqq{} finden sich die Ratings unserer Evaluation im JSON-Format. Pro Query ist eine Datei vorhanden, die für alle gefundenen Texte festhält, ob dieses Ergebnis relevant war (= 1) oder nicht (= 0). Beim Export des Datenbank-Inhalts werden auch diese Datensätze mit den aktuellsten Werten überschrieben (siehe \autoref{sec:Export} \glqq \nameref{sec:Export}\grqq).


\subsection{Fazit der Evaluation}

Im Wesentlichen haben die Iterationen (mit einer Ausnahme) zur Verbesserung der Scores beigetragen, wenn man die Änderung zur vorherigen Iteration betrachtet. Die implizite Phrasensuche jedoch verschlechtert das Ergebnis beträchtlich und auch danach kommen die Iterationen trotz Steigerung nicht mehr auf so erfolgreiche Scores wie zuvor. Dennoch haben die anderen Iterationen stets bessere Ergebnisse erzielt als ihre Vorgänger. Insbesondere die Iteration, bei der erstmals die Suche auf die Titel der Texte ausgeweitet wird, erzielt gute Ergebnisse und die höchsten Scores unserer Evaluation.


\section{Nutzung}

\subsection{Voraussetzungen}

Das Projekt wird mithilfe von Docker bereitgestellt, deshalb muss auf dem ausführenden System Docker und docker-compose installiert bzw. eingerichtet werden. Dieser Schritt variiert je nach System. Zu beachten ist, dass der docker-daemon laufen muss. Die Dokumentation von Docker findet sich unter: https://docs.docker.com/install/


\subsection{Getting started}

Durch die Nutzung von Docker ist die Installation und Ausführung des \glqq Religioracles\grqq{} vergleichsweise einfach. Das Projekt wurde ausschließlich unter Linux getestet, sollte aber auf allen Systemen funktionieren, auf denen Docker unterstützt wird.

Sobald Docker und docker-compose erfolgreich installiert bzw. eingerichtet wurden, sind folgende Schritte sind für die Ausführung zu befolgen:

\begin{itemize}
\item Herunterladen des Projektes. Das kann beispielsweise mit\newline
`git clone https://gitlab.com/datenstaubsauger/ir\_project.git` erreicht werden.
\item Starten einer Terminalsitzung im Wurzelverzeichnis des Projekts.
\item Ausführen des Befehls `docker-compose up`. Hierfür werden normalerweise root-Rechte benötigt, dies variiert aber je nach Konfiguration des Systems.
\item Nun ist der Webserver unter http://localhost:8000 erreichbar.
\end{itemize}

Genauere Informationen finden sich auch in der \glqq README.md\grqq.


\subsection{Online-Version}

Eine Online-Version des Projektes ist auch unter http://ir.twinter.eu:8000 erreichbar.


\end{document}