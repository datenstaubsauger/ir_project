# Help

We use an environment based on docker-compose. The different servers each have their own docker container.
Below are a couple of (hopefully) helpful commands for docker, docker-compose and a couple of other tools used in this project.

## docker

- `docker help`
- `docker ps -a` to show all containers on the system
- `docker exec -it <name or id> bash` to open a terminal inside of a running container
- `docker stop <name oder id>` to shut down a container
- `docker remove <name oder id>` to remove a specific container
- `docker system prune -a` to purge all currently unused docker images, network, volumes, …

## docker-compose

Tool for easier handling and configuration of multiple docker containers in a single project

- `docker-compose help`
- `docker-compose up` to start whats defined in docker-compose.yml
- `docker-compose up -d` to do the above but in detached mode
- `docker-compose down` to stop execution in detached mode
- `docker-compose run <container id> <command>` to run a single command in a specific container
- `docker-compose exec <id of container> bash` to open a terminal session in the specified container
- `docker-compose build` the rebuild the containers (for example after adding more packages or changing the container configuration)
- `docker-compose build --no-cache` to rebuild the containers (for example after changing some python code)

## django

The following commands have to be executed in /code/ir_project in the container.

- regenerate databse migrations if the model classes have changed with `python manage.py makemigrations` and apply them with `python manage.py migrate`. See https://docs.djangoproject.com/en/2.1/topics/migrations/ for details.

## pipenv

- `pipenv --help`
- `pipenv install` to initially install the project dependencies and set up the virtual environment
- `pipenv shell` gets you a shell in the virtual environment
- `pipenv run <Befehl>` runs single commands
- `pipenv update` to apply changes to the pipfile
