# IR-project

## installation and startup

Everything described below is a setup for development and testing. Do not use this in production!

### installation

It is recommended to do this on linux (this should work on windows as well but will likely be more complicated).

1. install docker and docker-compose
2. set up docker on your system ([guide for all supported platforms](https://docs.docker.com/install/#supported-platforms), [guide for arch linux](https://wiki.archlinux.org/index.php/Docker), [run the docker daemon automatically on startup](https://docs.docker.com/install/linux/linux-postinstall/#configure-docker-to-start-on-boot)), you will likely need root privileges
3. have the project files available somewhere on your system (for example by cloning it with `git clone https://gitlab.com/datenstaubsauger/ir_project.git`)
4. make sure the following files are executable (relative to the projects root directory): `./ir_project/manage.py`, `./religioese_texte/solr_startup.sh` and `./religioese_texte/parser/parser.py`

The commands you will be executing will look roughly like this (tested on a fresh installation of Ubuntu 18.04 LTS):
```
# install docker and ensure it's active and running
sudo apt install docker-compose
sudo systemctl enable docker
sudo systemctl start docker

# make sure we're in the home folder
cd ~

# clone the project
git clone https://gitlab.com/datenstaubsauger/ir_project.git

# move into the projects folder
cd ir_project/

# start the containers and stuff
sudo docker-compose up
```

### run the project

1. get a terminal session in the root folder of the project
2. run `docker-compose up` (you may need elevated privileges) to start the containers, configure the networking and import all required data
3. the project is ready to use when you see the following two messages:
    - `Starting development server at http://0.0.0.0:8000/` and `Quit the server with CONTROL-C.` from then web container (usually prefixed with `web_1   | `)
    - `solr is ready` from the solr container (usually prefixed with `solr_1  | `)

You can reach the web server on http://localhost:8000 and the solr server on http://localhost:8983.

### evaluate the search engines performance

The project has a builtin evaluation mechanism that will automatically import the evaluation queries from a file defined in settings.
You must not remove queries after you've started evaluating as this will change the the rating of older iterations. You may however add more queries (but remember to import the queries manually with a GET request to `localhost:8000/tool/insert_queries` after you've made changes).

### usage of internal tools in the project

The following are built-in tools bound to urls:

- `tool/insert_queries` inserts queries from the file defined in settings
- `tool/update_scores` recalculates all scores
- `tool/export_db` exports all everything in the database to a set of files in folders defined in settings
- `tool/import_db` forces to import the data from the files defined in settings to the database
- `tool/flush_db/yes/really/delete/everything` deletes everything in the database
- `tool/rate_everything` rates everything randomly

### notes on external tools used in the project

see HELP.md

## License

This project is licensed under the GNU Affero General Public License v3.0 (AGPLv3). See LICENSE.md for details.
