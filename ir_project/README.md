
# Apps

- [`web_frontend`](./web_frontend):
  User facing search and result interface.
- [`solr_backend`](./solr_backend):
  Simple [solr](http://lucene.apache.org/solr/) abstraction.
