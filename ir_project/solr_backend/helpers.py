import collections
from typing import Dict, OrderedDict
import requests
import re

from django.conf import settings

"""

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Important notes in the values returned by the following functions:
- the id's must not contain any white space
- try not to change the signature of these functions as other parts of the program depend on them

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

"""


class SolrException(Exception):
    pass


class SolrConnectionError(SolrException):
    pass


class SolrRetrievalError(SolrException):
    pass


# list of field we retrieve with the results from solr
SOLR_FIELD_LIST = 'id,score,title,text,year_range.*,unique_name,origin'
# Parameters for highlighting results
SOLR_HIGHLIGHTER_PARAMS = 'hl=on&hl.method=unified&hl.fl=text,title&hl.simple.pre=START_OF_HIGHLIGHT' \
                          '&hl.simple.post=END_OF_HIGHLIGHT&hl.snippets=10&hl.usePhraseHighlighter=true'


def apply_highlighting(r, highlightings):
    """Surround Solr's recommended highlightings with an appropriate html tag in the title and text fields."""
    id_ = r['id']
    if id_ in highlightings:
        h = highlightings[id_]
        if 'text' in h:
            for entry in h['text']:
                orig = entry. \
                    replace('START_OF_HIGHLIGHT', ''). \
                    replace('END_OF_HIGHLIGHT', '')
                new = entry. \
                    replace('START_OF_HIGHLIGHT', '<span class="highlighting">'). \
                    replace('END_OF_HIGHLIGHT', '</span>')
                r['text'][0] = r['text'][0].replace(orig, new)
        if 'title' in h:
            for entry in h['title']:
                orig = entry. \
                    replace('START_OF_HIGHLIGHT', ''). \
                    replace('END_OF_HIGHLIGHT', '')
                new = entry. \
                    replace('START_OF_HIGHLIGHT', '<span class="highlighting">'). \
                    replace('END_OF_HIGHLIGHT', '</span>')
                r['title'][0] = r['title'][0].replace(orig, new)
    return r


def split_query(query: str) -> [(str, bool)]:
    """
    Split a query into it's whitespace-separated parts, respecting phrases. This function returns a list of tuples
    containing the word/phrase and True if the str is a phrase, False if just a word.
    """
    # TODO: Ignoring differences between ' and ", does it matter?
    phrases = []
    current_phrase = ''
    inside_quotes = False
    for char in query:
        current_phrase += char
        # Iterate over all characters
        if char in '"\'':
            # If inside a non-empty phrase and detecting a closing quote
            if inside_quotes and current_phrase.strip() != '':
                phrases.append((current_phrase.strip(), True))
                current_phrase = ''
            inside_quotes = not inside_quotes
        # not inside a phrase the delimiter should be space
        if not inside_quotes and char == ' ' and current_phrase.strip() != '':
            phrases.append((current_phrase.strip(), False))
            current_phrase = ''
    if current_phrase.strip() != '':
        phrases.append((current_phrase.strip(), inside_quotes))
    return phrases


def preprocess_query(query: str) -> str:
    """ Perform query preprocessing """
    
    query = query.strip()
    final_query = ''
    
    # Search the text
    phrases = split_query(query)
    current_keyword_sequence = ''
    for (part, is_phrase) in phrases:
        if is_phrase:
            if current_keyword_sequence.strip() != '':
                current_keyword_sequence = current_keyword_sequence.strip()
                final_query += f' title:"{current_keyword_sequence}"~2^4 text:"{current_keyword_sequence}"~2^2'
            current_keyword_sequence = ''
            final_query += f' title:{part}~2^4  text:{part}~2^2'
        else:
            # add fuzzy and normal search keywords in text
            final_query += f' text:{part}~1^0.5 text:{part}'
            current_keyword_sequence += f' {part}'
    if current_keyword_sequence.strip() != '':
        current_keyword_sequence = current_keyword_sequence.strip()
        final_query += f' title:"{current_keyword_sequence}"~2^4 text:"{current_keyword_sequence}"~2^2'
    return final_query


def get_results(query: str, amount: int = settings.ITEMS_PER_PAGE) -> OrderedDict[str, Dict[str, str]]:
    """
        Search the Solr backend using the given query as a basis. The query is transformed before usage. The optional parameter
        can be used to limit the number of returned documents.
    """
    # TODO: catch SolrConnectionError in functions calling this
    
    query = preprocess_query(query)
    url = f'http://{settings.SOLR_HOST}/solr/alle/select?q={query}&fl={SOLR_FIELD_LIST}&rows={amount}&{SOLR_HIGHLIGHTER_PARAMS}'
    # Catch possible broken url and backend connection
    try:
        response = requests.get(url)
    except requests.ConnectionError:
        raise SolrConnectionError
    
    # raise exception if status is something other than ok
    if response.status_code not in range(200, 300):
        print('solr error message:\n' + response.content.decode())
        raise SolrConnectionError(f'unexpected http status code: {response.status_code}, {response.content}')
    
    results = collections.OrderedDict()
    solr_json = response.json()
    max_score = solr_json['response']['maxScore']
    highlightings = solr_json['highlighting']
    for r in solr_json['response']['docs']:
        r = apply_highlighting(r, highlightings)
        details = {
            'title':  r['title'][0],
            'text':   r['text'][0],
            'score':  r['score'] / max_score,
            'origin': r['origin'][0],
        }
        
        # add optional attributes
        if 'year_range.start' in r and 'year_range.end' in r:
            details['year_range_start'] = r['year_range.start'][0]
            details['year_range_end'] = r['year_range.end'][0]
            details['year_range_middle'] = float(details['year_range_start'] + details['year_range_end']) / 2.
        
        results[r['unique_name'][0]] = details
    return results


def get_document_by_id(id_: str, score_inject: float = None, highlighting_query: str = None) -> Dict[str, str]:
    url = f'http://solr:8983/solr/alle/select?q=unique_name:{id_}&fl={SOLR_FIELD_LIST}&rows=1'
    
    # add highlighting options if it is used
    if highlighting_query is not None:
        url += f'&{SOLR_HIGHLIGHTER_PARAMS}&hl.q={highlighting_query}'
    
    # Catch possible broken url and backend connection
    try:
        response = requests.get(url)
    except requests.ConnectionError:
        raise SolrConnectionError
    
    # raise exception if status is something other than ok
    if response.status_code not in range(200, 300):
        print('solr error message:\n' + response.content.decode())
        raise SolrConnectionError(f'unexpected http status code: {response.status_code}, {response.content}')
    
    response = requests.get(url).json()
    if int(response['response']['numFound']) < 1:
        raise SolrRetrievalError(f'no document with name "{id_}" found')
    
    if highlighting_query is not None:
        highlightings = response['highlighting']
        result = apply_highlighting(response['response']['docs'][0], highlightings)
    else:
        result = response['response']['docs'][0]
    document = {
        'title':  result['title'][0],
        'text':   result['text'][0],
        'score':  score_inject,
        'origin': result['origin'][0],
    }
    
    # add optional attributes
    if 'year_range.start' in result and 'year_range.end' in result:
        document['year_range_start'] = result['year_range.start'][0]
        document['year_range_end'] = result['year_range.end'][0]
    
    return document
