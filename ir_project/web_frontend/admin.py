from django.contrib import admin
from web_frontend.models import EvalQuery, Iteration, Rating, Result, QueryScore


admin.site.register(EvalQuery)
admin.site.register(Rating)
admin.site.register(Iteration)
admin.site.register(Result)
admin.site.register(QueryScore)
