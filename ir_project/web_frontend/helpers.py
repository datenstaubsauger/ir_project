import json
import os
import shutil

from django.conf import settings
from django.core import serializers
from django.db.utils import OperationalError
from django.db import transaction

from web_frontend import models


def on_startup():
    """
    This will be called when this module is initialized.
    Keep in mind that this happen during the startup process of django so some
        things may not be available at this point.
    """
    
    print('running startup tasks')
    import_data()


def recalculate_scores():
    iterations = models.Iteration.objects.all()
    for i in iterations:
        i.update_score(True)
        i.save()


def _ensure_path_exists(path: str):
    """Create folder if it doesn't exist."""
    
    if not os.path.exists(path):
        os.makedirs(path)


def _remove_folder_with_content(path: str):
    try:
        shutil.rmtree(path)
    except FileNotFoundError:
        pass


def export_data(clear_old: bool = True):
    # remove old files
    if clear_old:
        _remove_folder_with_content(settings.DB_EXPORT_DIR_BASE)
        _remove_folder_with_content(settings.DB_EXPORT_DIR_RESULTS)
        _remove_folder_with_content(settings.DB_EXPORT_DIR_RATINGS)
        _remove_folder_with_content(settings.DB_EXPORT_DIR_QUERY_SCORES)
    
    # export data from the database
    export_iterations()
    iterations = models.Iteration.objects.all()
    for q in models.EvalQuery.objects.all():
        for i in iterations:
            export_results(i, q)
        export_ratings(q)
    for i in iterations:
        export_query_scores(i)


def import_data(force: bool = False):
    # defining the import as atomic since we want to commit the db after the whole data set has been imported
    with transaction.atomic():
        try:
            # import all the data
            import_evaluation_queries(force)
            import_iterations(force)
            iterations = models.Iteration.objects.all()
            for q in models.EvalQuery.objects.all():
                for i in iterations:
                    import_results(i, q, force)
                import_ratings(q, force)
            for i in iterations:
                import_query_scores(i, force)
            
            # update all iteration scores, query_score scores are updated on import
            for i in iterations:
                i.update_score(cascade=False)
        
        # testing if we should import the data
        except OperationalError:
            print('error during db access. Try again later.')


def import_evaluation_queries(force: bool = False):
    """ Imports evaluation Queries if there are none in the database of if forced. """
    
    if models.EvalQuery.objects.all().count() == 0 or force:
        print('importing evaluation queries')
        try:
            with open(settings.USE_CASES_FILE, "r") as use_cases:
                data = json.loads(use_cases.read())
                for d in data:
                    # put the data from json into the database
                    q = models.EvalQuery(
                            id=int(d['topic_id']),
                            query=d['query'],
                            description=d['description'],
                            narrative=d['narrative']
                    )
                    if models.EvalQuery.objects.filter(id=q.id).exists():
                        q.save(force_update=True)
                    else:
                        q.save(force_insert=True)
        except OSError:
            print('could not read file with evaluation queries')
    else:
        print('skipping import of evaluation queries')


ITERATIONS_FILE_NAME = 'iterations.json'


def export_iterations():
    _ensure_path_exists(settings.DB_EXPORT_DIR_BASE)
    
    # write all serialized version of all iterations to one file
    iterations = models.Iteration.objects.all()
    with open(os.path.join(settings.DB_EXPORT_DIR_BASE, ITERATIONS_FILE_NAME), 'w+') as outfile:
        serializers.serialize('json', iterations, stream=outfile, fields=('id', 'name', 'changes', 'date'))


def import_iterations(force: bool = False):
    try:
        with open(os.path.join(settings.DB_EXPORT_DIR_BASE, ITERATIONS_FILE_NAME), 'r') as infile:
            iterations_from_file = serializers.deserialize('json', infile.read())
            iterations_from_file = list(iterations_from_file)
            if force or len(iterations_from_file) > models.Iteration.objects.count():
                print('importing iterations')
                for i in iterations_from_file:
                    iteration = models.Iteration(
                            id=i.object.id,
                            name=i.object.name,
                            changes=i.object.changes,
                            date=i.object.date
                    )
                    if models.Iteration.objects.filter(id=iteration.id).exists():
                        iteration.save(disable_buffering=True, force_update=True)
                    else:
                        iteration.save(disable_buffering=True, force_insert=True)
            
            else:
                print('skipping import of iterations')
    except OSError:
        print('could not read iterations file')


RESULTS_FILE_NAME = 'results_iteration_{}_query_{}.json'


def export_results(iteration: models.Iteration, query: models.EvalQuery):
    """ Save all results for this query for this iteration. Does nothing if there are no results. """
    
    _ensure_path_exists(settings.DB_EXPORT_DIR_RESULTS)
    
    # load results, only write to file if there are any
    results = models.Result.objects.filter(iteration=iteration, query=query).all()
    if len(results) == 0:
        return
    
    # save serialized version of the results for this query in this iteration
    path = os.path.join(settings.DB_EXPORT_DIR_RESULTS, RESULTS_FILE_NAME.format(iteration.id, query.id))
    with open(path, 'w+') as outfile:
        serializers.serialize('json', results, stream=outfile)


def import_results(iteration: models.Iteration, query: models.EvalQuery, force: bool = False):
    # only import results if there are none in the database or if forced
    if force or models.Result.objects.filter(iteration=iteration, query=query).count() == 0:
        try:
            path = os.path.join(settings.DB_EXPORT_DIR_RESULTS, RESULTS_FILE_NAME.format(iteration.id, query.id))
            with open(path, 'r') as infile:
                print(f'importing results for iteration {iteration.id} and query {query.id}')
                results_from_file = serializers.deserialize('json', infile.read())
                for r in results_from_file:
                    result, created = models.Result.objects.get_or_create(
                            iteration=iteration,
                            query=query,
                            position=r.object.position,
                            defaults={
                                'score':       r.object.score,
                                'document_id': r.object.document_id
                            }
                    )
                    if not created:
                        # update the object if it did exist
                        result.score = r.object.score
                        result.document_id = r.object.document_id
                        result.save()
        
        except OSError:
            print(f'no results for iteration {iteration.id} and query {query.id} found')
    else:
        print(f'skipping import of results for iteration {iteration.id} and query {query.id}')


RATINGS_FILE_NAME = 'ratings_query_{}.json'


def export_ratings(query: models.EvalQuery):
    """ Save all ratings we have for the given query. Does nothing if we don't have any. """
    
    _ensure_path_exists(settings.DB_EXPORT_DIR_RATINGS)
    
    # load results, only write to file if there are any
    ratings = models.Rating.objects.filter(query=query).all()
    if len(ratings) == 0:
        return
    
    # collect the ratings
    ratings_list = []
    for r in ratings:
        ratings_list.append({
            'topic_id': str(r.query.id),
            'document_id': r.document_id,
            'relevance': r.rating,
        })
    
    # serialize it and save it to file
    path = os.path.join(settings.DB_EXPORT_DIR_RATINGS, RATINGS_FILE_NAME.format(query.id))
    with open(path, 'w+') as outfile:
        json.dump(ratings_list, outfile)


def import_ratings(query: models.EvalQuery, force: bool = False):
    try:
        path = os.path.join(settings.DB_EXPORT_DIR_RATINGS, RATINGS_FILE_NAME.format(query.id))
        with open(path, 'r') as infile:
            ratings_from_file = json.load(infile)
            if force or len(ratings_from_file) > models.Rating.objects.filter(query=query).count():
                print(f'importing ratings for query {query.id}')
                for r in ratings_from_file:
                    rating, created = models.Rating.objects.get_or_create(
                            query=query,
                            document_id=r['document_id'],
                            defaults={'rating': r['relevance']}
                    )
                    if not created:
                        rating.rating = r['relevance']
                        rating.save()
            else:
                print(f'skipping import of results for query {query.id}')
    
    except OSError:
        print(f'could not read results file for query {query.id}')


QUERY_SCORE_FILE_NAME = 'query_scores_iteration_{}.json'


# noinspection DuplicatedCode
def export_query_scores(iteration: models.Iteration):
    """
    Export Query score objects. This isn't necessary for the scores themselves (these aren't saved and will be
        recalculated on import) but to save the queries used in each iteration.
    """
    
    _ensure_path_exists(settings.DB_EXPORT_DIR_QUERY_SCORES)
    
    query_scores = models.QueryScore.objects.filter(iteration=iteration).all()
    if len(query_scores) == 0:
        return
    
    path = os.path.join(settings.DB_EXPORT_DIR_QUERY_SCORES, QUERY_SCORE_FILE_NAME.format(iteration.id))
    with open(path, 'w+') as outfile:
        serializers.serialize('json', query_scores, stream=outfile, fields=('query', 'iteration'))


def import_query_scores(iteration: models.Iteration, force: bool = False):
    try:
        path = os.path.join(settings.DB_EXPORT_DIR_QUERY_SCORES, QUERY_SCORE_FILE_NAME.format(iteration.id))
        with open(path, 'r') as infile:
            query_scores_from_file = serializers.deserialize('json', infile.read())
            if force or models.QueryScore.objects.filter(iteration=iteration).count() == 0:
                for q in query_scores_from_file:
                    query_score, created = models.QueryScore.objects.get_or_create(
                            iteration=iteration,
                            query_id=q.object.query.id
                    )
                    query_score.update_score()
            else:
                print(f'skipping import of query score objects for iteration {iteration.id}')
    except OSError:
        print(f'could not read query score object file for iteration {iteration.id}')
