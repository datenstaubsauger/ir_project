import math

from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction

from solr_backend.helpers import get_results


# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
# remember to run `python manage.py makemigrations` when changing something in here
#
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


class EvalQuery(models.Model):
    """
    A query used in the evaluation. Also contains what we expect so see on execution.
    """

    id = models.AutoField(primary_key=True)
    query = models.TextField(
            help_text='the query exactly as it would be entered in the search bar'
    )
    description = models.TextField(
            blank=True,
            help_text='optional text with a short description of the query'
    )
    narrative = models.TextField(
            blank=True,
            help_text='optional text detailing what kind of results we\'re expecting'
    )
    number = models.SmallIntegerField(
            null=True,
            help_text='The number of the queries in case they are numbered'
    )
    
    def __str__(self):
        return self.query


class Iteration(models.Model):
    """
    Represents an iteration in the tuning process of the search engine.
    """
    
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    changes = models.TextField(
            blank=True,
            help_text='optional bit of text detailing the changes made in this iteration'
    )
    date = models.DateTimeField(
            auto_now_add=True,
            help_text='creation date of the iteration'
    )
    score_chronological = models.FloatField(
            null=True, editable=False,
            help_text='calculated score of this iteration for chronological display'
    )
    score_ndcg = models.FloatField(
            null=True, editable=False,
            help_text='calculated score for this iteration for "regular" ranked display'
    )
    
    def __str__(self):
        return self.name
    
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None, disable_buffering=False):
        """
        Override of built in save() that buffers the results of all evaluation queries on creation.
        This behaviour may be disabled by settings the disable_buffering parameter to True.
        """
        
        # check if we're handling a new entry (id is None or doesn't exist)
        new_entry = True
        if self.id is not None and Iteration.objects.filter(id=self.id).exists():
            new_entry = False
        
        super().save(force_insert, force_update, using, update_fields)
        
        if new_entry and not disable_buffering:
            # new iteration created, save current query results
            with transaction.atomic():
                for q in EvalQuery.objects.all():
                    # execute the query and save the results
                    query_results = get_results(q.query)
                    i = 0
                    for id_, r in query_results.items():
                        result = Result(query=q, iteration=self, position=i, document_id=id_, score=r['score'])
                        result.save()
                        i += 1
                    
                    # create the QueryScore object for the query and iteration
                    query_score = QueryScore(query=q, iteration=self)
                    query_score.save()
                    query_score.update_score()
            
            # update the score for the whole iteration
            self.update_score(cascade=False)
    
    def update_score(self, cascade: bool = True):
        # get all QueryScore objects of this iteration
        query_scores = QueryScore.objects.filter(iteration=self).all()
        for q in query_scores:
            if cascade:
                q.update_score()

        total_chronological = 0
        total_ndcg = 0
        for q in query_scores:
            # don't calculate our own score if not all queries have scores
            if q.score_chronological is None or q.score_ndcg is None:
                self.score_chronological = None
                self.score_ndcg = None
                self.save()
                return
            
            total_chronological += q.score_chronological
            total_ndcg += q.score_ndcg
        try:
            self.score_chronological = total_chronological / len(query_scores)
            self.score_ndcg = total_ndcg / len(query_scores)
        except ZeroDivisionError:
            self.score_chronological = 0
            self.score_ndcg = 0
        self.save()
    
    
class QueryScore(models.Model):
    """
    The calculated score of a query/iteration pair. Also saves yet to be scored pairs when score is not set.
    This gets created automatically when a new iteration is created
    """

    query = models.ForeignKey(EvalQuery, on_delete=models.CASCADE)
    iteration = models.ForeignKey(Iteration, on_delete=models.CASCADE)
    score_chronological = models.FloatField(
            null=True, editable=False,
            help_text='computed score for the query-iteration pair for chronological display'
    )
    score_ndcg = models.FloatField(
            null=True, editable=False,
            help_text='computed score for the query-iteration pair for "regular" ranked display'
    )
    
    def __str__(self):
        return 'Score for query "' + self.query.query + '" (iteration "' + self.iteration.name + '")'
    
    def update_score(self):
        # collect ratings
        results = Result.objects.filter(query=self.query, iteration=self.iteration).order_by('position').all()
        ratings_list = []
        for r in results:
            # check if the result is already rated
            try:
                x = Rating.objects.get(query=self.query, document_id=r.document_id).rating
                ratings_list += [x]
                r.rating = x  # inject the rating into the result object
            except ObjectDoesNotExist:
                # one document is not rated so we won't calculate the score
                self.score_chronological = None
                self.score_ndcg = None
                self.save()
                return
        
        # calculate scores
        total_chronological = 0
        dcg = 0
        dcg_ideal = 0
        ratings_ideal = sorted(ratings_list, reverse=True)
        i = 0
        for r in results:
            # accumulate weighted ratings for the chronological view
            total_chronological += r.rating * r.score
            
            # calculate DCG (discounted cumulative gain) and ideal DCG
            dcg += (math.pow(2, r.rating) - 1) / math.log2(1 + r.position + 1)  # NDCG index starts at 1, ours at 0
            dcg_ideal += (math.pow(2, ratings_ideal[i]) - 1) / math.log2(1 + i + 1)
        
        # calculate final scores
        try:
            self.score_chronological = total_chronological / len(results)
            self.score_ndcg = dcg / dcg_ideal
        except ZeroDivisionError:
            self.score_chronological = 0
            self.score_ndcg = 0
        self.save()
    
    
class Rating(models.Model):
    """
    The relevance rating for a single query-document pair.
    """
    
    query = models.ForeignKey(EvalQuery, on_delete=models.CASCADE)
    document_id = models.TextField()
    rating = models.IntegerField(
            help_text='the rating given to this query-result pair during evaluation'
    )
    
    def __str__(self):
        return self.document_id + ' for query "' + self.query.query + '"'
    

class Result(models.Model):
    """
    Saves the results of the query for a given iteration. This allows us to view the results of earlier iterations.
    """
    
    query = models.ForeignKey(EvalQuery, on_delete=models.CASCADE)
    iteration = models.ForeignKey(Iteration, on_delete=models.CASCADE)
    position = models.PositiveSmallIntegerField(
            help_text='the position as returned by solr'
    )
    score = models.FloatField(
            help_text='the score as calculate by solr normalized by the max score of all simultaneous results'
    )
    document_id = models.TextField(
            help_text='the unique id of the document, constant between instantiations of solr'
    )
    
    def __str__(self):
        return 'pos. ' + str(self.position) + ' for query "' + self.query.query \
               + '" (iteration "' + str(self.iteration.name) + '")'
