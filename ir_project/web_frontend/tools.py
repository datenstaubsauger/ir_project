import random

from django.http.response import HttpResponse

from web_frontend import models, helpers


# this contains utility functions connected to url's


def insert_queries(request):
    helpers.import_evaluation_queries(True)
    return HttpResponse('Queries have been inserted into the database.')


def rate_everything(request):
    unfinished_query_sets = models.QueryScore.objects.filter(score_chronological__isnull=True).all()
    for q in unfinished_query_sets:
        # load the result set for the query-iteration pair and set the ratings
        query = q.query
        iteration = q.iteration
        result_set = models.Result.objects.filter(query=query, iteration=iteration).all()
        for r in result_set:
            if not models.Rating.objects.filter(query=query, document_id=r.document_id).exists():
                models.Rating(query=query, document_id=r.document_id, rating=random.randint(0, 1)).save()
        
        # update the score of the query set
        q.update_score()
    
    return HttpResponse('Every unrated query-document pair has been rated randomly with 0 or 1.')


def update_scores(request):
    helpers.recalculate_scores()
    return HttpResponse('All scores have been recalculated.')


def export_db(request):
    helpers.export_data()
    return HttpResponse('Database contents have been exported')


def import_db(request):
    helpers.import_data(force=True)
    return HttpResponse('Exported files have been imported')


def flush_db(request):
    # remove iterations
    iterations = models.Iteration.objects.all()
    for i in iterations:
        i.delete()
    
    # remove queries
    queries = models.EvalQuery.objects.all()
    for q in queries:
        q.delete()
        
    return HttpResponse('Everything in the database has been deleted')
