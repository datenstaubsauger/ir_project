from django.urls import path
from django.views.generic import TemplateView

from web_frontend import views, tools, helpers


# register function with stuff that should be executed on django startup
helpers.on_startup()


index_view = TemplateView.as_view(template_name='web_frontend/index.html')
urlpatterns = [
    path('', index_view, name='index'),
    path('index', index_view),
    path('search', views.search, name='search'),
    path('evaluate', views.evaluate, name='evaluate'),
    path('iterations', views.iterations_list, name='iterations list'),
    path(
            'iterations/<int:iteration_id>/query/<int:query_id>',
            views.iterations_single_query_view,
            name='iterations single query view'
    ),
    path('iterations/<int:iteration_id>', views.iterations_view, name='iterations view'),
    
    # everything starting with 'api/' works with JSON messages and is not safe for human consumption
    path('api/receive_ratings', views.api_receive_ratings, name='api receive ratings'),
    path('api/update_scores', views.api_update_scores, name='api update scores'),
    path('api/iteration/create', views.api_iterations_create, name='api iteration create'),
    path('api/iteration/delete/<str:id_>', views.api_iteration_delete, name='api iteration delete'),
    
    # everything under '/tool' will not be connected through the website but is reachable through its url
    path('tool/insert_queries', tools.insert_queries),
    path('tool/rate_everything', tools.rate_everything),
    path('tool/update_scores', tools.update_scores),
    path('tool/export_db', tools.export_db),
    path('tool/import_db', tools.import_db),
    path('tool/flush_db/yes/really/delete/everything', tools.flush_db)
]
