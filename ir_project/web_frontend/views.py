import json
import random
from collections import OrderedDict
from typing import Optional

from django.shortcuts import render, redirect
from django import urls
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.http import require_GET, require_POST
from django.http.response import HttpResponse, JsonResponse, Http404
from django.http.request import HttpRequest
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings

from web_frontend import models, helpers
from solr_backend.helpers import get_results, get_document_by_id


@require_GET
def search(request: HttpRequest):
    if 'query' not in request.GET:
        return redirect('index')
    
    query = str(request.GET['query']).strip()
    if query == '':
        return redirect('index')
    
    # check if we're displaying the page in chronological order
    chronological_order = False
    if 'chronological_order' in request.GET and request.GET['chronological_order'] == 'on':
        chronological_order = True
    
    results = get_results(query)
    
    def _key_func(item):
        """
        Helper function used in sorting the documents by creation date.
        Return average estimated creation date or preset default.
        """
        if 'year_range_middle' in item[1]:
            return item[1]['year_range_middle']
        else:
            return settings.YEAR_RANGE_DEFAULT
    
    if chronological_order:
        # sort results by creation date
        results = results.items()
        results = sorted(results, key=_key_func)
        results = OrderedDict(results)
    
    return render(
            request,
            'web_frontend/results.html',
            {
                'results': results,
                'query': query,
                'chronological_order': chronological_order,
                'truncate_amount': settings.RESULTS_WORD_COUNT_CUTOFF,
                'truncate_threshold': settings.RESULTS_WORD_COUNT_CUTOFF + settings.RESULTS_WORD_COUNT_TOLERANCE,
            }
    )


def _collect_result_data(query: models.EvalQuery, iteration: models.Iteration, randomize: bool = False):
    result_set = models.Result.objects.filter(query=query, iteration=iteration)
    if randomize:
        result_set = result_set.order_by('?').all()
    else:
        result_set = result_set.order_by('position').all()
        
    documents = OrderedDict()
    for r in result_set:
        documents[r.document_id] = get_document_by_id(
                r.document_id,
                score_inject=r.score,
                highlighting_query=query.query
        )
        
        try:
            documents[r.document_id]['rating'] = models.Rating.objects.get(
                    document_id=r.document_id,
                    query_id=query.id
            ).rating
        except ObjectDoesNotExist:
            pass
    
    return documents


@require_GET
@ensure_csrf_cookie  # make sure that we have the CSRF cookie set to be able to use in ajax requests later
def evaluate(request: HttpRequest):
    # count the result sets with incomplete rating (i.e. undefined score)
    query_set = models.QueryScore.objects.filter(score_chronological__isnull=True)
    remaining_queries = query_set.count()
    
    if remaining_queries > 0:
        # load eval query and iteration data
        query_score = query_set[random.randrange(remaining_queries)]
        query = query_score.query
        iteration = query_score.iteration
        
        # load documents
        documents = _collect_result_data(query, iteration, randomize=True)
        
        return render(request, 'web_frontend/evaluate.html', {
            'remaining_queries': remaining_queries,
            'documents':         documents,
            'query':             query,
            'iteration':         iteration,
        })
    else:
        return render(request, 'web_frontend/evaluate.html', {
            'remaining_queries': 0,
        })


@require_GET
@ensure_csrf_cookie
def iterations_list(request):
    """
    Displays the list of iterations, their names, scores, etc.
    """
    
    # update the scores (without cascading, assuming the scores for each result set are up to date)
    iterations = models.Iteration.objects.all()
    prev_score_chronological = None
    prev_score_ndcg = None
    for i in iterations:
        i.update_score(False)
        
        def calculate_relative_difference(old: float, new: float) -> Optional[float]:
            if old is None or new is None:
                return None
            if old == 0:
                return float('inf')
            return (new - old) / old
        
        # inject changes relative to previous iteration
        i.score_change_chronological = calculate_relative_difference(prev_score_chronological, i.score_chronological)
        i.score_change_ndcg = calculate_relative_difference(prev_score_ndcg, i.score_ndcg)
        prev_score_chronological = i.score_chronological
        prev_score_ndcg = i.score_ndcg
    
    return render(request, 'web_frontend/iterations.html', {'iterations': iterations})


@require_GET
def iterations_view(request, iteration_id):
    try:
        iteration = models.Iteration.objects.get(id=iteration_id)
        iteration.update_score()
        query_scores = models.QueryScore.objects.filter(iteration=iteration).all().prefetch_related('query')
        return render(request, 'web_frontend/iterations_view.html', {
            'iteration':    iteration,
            'query_scores': query_scores,
        })
    except ObjectDoesNotExist:
        raise Http404('iteration does not exist.')


@require_GET
def iterations_single_query_view(request, iteration_id, query_id):
    try:
        iteration = models.Iteration.objects.get(id=iteration_id)
        query = models.EvalQuery.objects.get(id=query_id)
        return render(request, 'web_frontend/iteration_query_view.html', {
            'query':     query,
            'iteration': iteration,
            'documents': _collect_result_data(query, iteration),
            'next_page': urls.reverse('iterations view', args=(iteration.id,)),
        })
    except ObjectDoesNotExist:
        raise Http404('iteration does not exist.')


@require_POST
def api_receive_ratings(request: HttpRequest):
    data = json.loads(request.body)
    query_id = data['query_id']
    iteration_id = data['iteration_id']
    
    # save the ratings
    for id_, rating in data['ratings'].items():
        try:
            r = models.Rating.objects.get(query_id=query_id, document_id=id_)
            if r.rating != rating:
                # the rating changed, update and save it
                r.rating = rating
                r.save()
                
                # get result objects for this result and update the scores of associated query sets and iterations
                relevant_results = models.Result.objects.filter(document_id=id_, query_id=query_id).all()
                for r in relevant_results:
                    # get related query score and iteration and update them
                    query_set = models.QueryScore.objects.get(query_id=query_id, iteration=r.iteration)
                    query_set.update_score()
                    r.iteration.update_score(False)
        
        except ObjectDoesNotExist:
            r = models.Rating(query_id=query_id, document_id=id_, rating=rating)
            r.save()
    
    # update the query score
    models.QueryScore.objects.get(query_id=query_id, iteration_id=iteration_id).update_score()
    
    return JsonResponse({'received data': data})


@require_POST
def api_update_scores(request):
    helpers.recalculate_scores()
    return JsonResponse(data={})


@require_POST
def api_iterations_create(request: HttpRequest):
    """
    Receive the data required to create a new iteration and create one.
    """
    
    data = json.loads(bytes.decode(request.body))
    name = data['name']
    changes = data['changes']
    models.Iteration(name=name, changes=changes).save()
    return JsonResponse(data={})


@require_POST
def api_iteration_delete(request, id_):
    models.Iteration.objects.get(id=id_).delete()
    return JsonResponse(data={})
