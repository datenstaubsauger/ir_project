#!/usr/bin/env bash

# apply migrations
python3 manage.py migrate --noinput

# start the test server
# !!! DO NOT USE THIS IN PRODUCTION !!!
python3 manage.py runserver 0.0.0.0:8000
