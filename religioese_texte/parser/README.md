# Parser

To run the parser, make sure to install all mentioned dependencies (`pipenv install`), then simply run
```console
$ ./parser.py
```

You might want to adjust the variables inside the script. I should have added comments to that by now.


# Year information

The year information file contains valid json information about the ~~guessed~~ assumed year of writing. It should look like this:
```json
{
    "Bibel": {
        "KEY": {
            "start": 1990,
            "end": 1991
        },
        ...
    },
    "Torah": { ... },
    "Koran": { ... },
}
```

The KEY depends on the category (`Bibel`, `Torah`, `Koran`):

- `Bibel`: Basename of the filepath without `(_[0-9]+)?.html` stripped from the end
- `Koran`: Basename of the filepath without extension
- `Torah`: Basename of the parent folder of the filepath without extension
