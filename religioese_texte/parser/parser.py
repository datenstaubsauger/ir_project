#!/usr/bin/env python3
# coding=utf-8

"""
USAGE:
simply run the script to parse the documents stored in ../

ARGUMENTS:
if no arguments are passed then the output path will be ../json
if on or more arguments are passed then the first argument will be interpreted as the desired output path
"""

# ~Probably won't~ Might just work under WINDOWS due to ~'/' and '\\'~ my awesomeness

from bs4 import BeautifulSoup, NavigableString
import json
import os
from os import path
from os.path import join as join_path
import sys
import re

# Top output directory for produced json files.
OUTPUT_DIRECTORY = '../json'
# Prefix for all directories (this file's folder)
BASE_PATH = path.dirname(path.realpath(__file__))
# File with year information content
YEAR_INFORMATION_FILE = join_path(BASE_PATH, 'year_info.json')
# Default year information entry, if none is found inside YEAR_INFORMATION_FILE
DEFAULT_YEAR_INFORMATION = {'start': None, 'end': None}


def filename_without_number(path):
    """Create an identifier by stripping _* from the back of the basename"""
    base = os.path.basename(path)
    good_parts = base.split('_')[:-1]
    name = '_'.join(good_parts)
    return name


def filename_without_extension(path):
    """Create an identifier by stripping all parent folders
    from the given path and removing the file extension"""
    base = os.path.basename(path)
    (name, _) = os.path.splitext(base)
    return name


def parent_dir_without_extension(path):
    """Create an identifier by getting the parent
    dir name without it's parents and extension"""
    parent = os.path.dirname(path)
    return filename_without_extension(parent)


def parse_to_soup(html):
    """Parse the given html to a BeautifulSoup object"""
    return BeautifulSoup(html, 'html.parser')


def read_year_information():
    """Read and json decode the YEAR_INFORMATION_FILE"""
    with open(YEAR_INFORMATION_FILE) as f:
        return json.load(f)


def get_all_html_files(folder_path):
    """Get a list of paths of all the html files in the
    given folder and their subfolders"""
    paths = os.listdir(folder_path)
    html_file_paths = []
    for path in paths:
        path = os.path.join(folder_path, path)
        if os.path.isfile(path) and path.endswith('.html'):
            html_file_paths.append(path)
        elif os.path.isdir(path):
            html_file_paths.extend(get_all_html_files(path))
    return html_file_paths


def write_object_to(obj, path):
    """Json encode the given object and write it to 'path'"""
    with open(path, 'w') as f:
        # Dump as valid ascii (escaped UTF8 chars)
        # json.dump(obj, f, indent=4)
        # Dump as invalid ascii (unescaped UTF8 chars)
        json.dump(obj, f, indent=4, ensure_ascii=False)


def has_all_classes(soup, classes):
    for class_ in classes:
        if 'class' in soup and not class_ in soup['class']:
            return False
    return True


def clean(soup, replace):
    if isinstance(soup, NavigableString) or isinstance(soup, str):
        return soup

    for repl in replace:
        if soup.name == repl['tag'] and has_all_classes(soup, repl['classes']):
            # create new tag
            tag = soup
            tag.name = repl['new_tag']
            tag['class'] = []
            for class_ in repl['new_classes']:
                tag['class'].append(class_)
            contents = []
            for el in soup.contents:
                contents.append(clean(el, replace))
            tag.contents = contents
            return str(tag)

    s = ''
    for el in soup.contents:
        s += clean(el, replace)
    return s


def format_koran_vns(entry):
    new_text = re.sub(r'\(([0-9]+)\.\)', r'<span class="number">\1</span>', entry['text'])
    entry['text'] = new_text
    return entry


def parse(profile):
    """Parse all files to valid json output"""
    print('parsing {}'.format(profile['id']))
    
    # Read year information if required for this profile
    year_file_content = read_year_information()
    year_information = year_file_content[profile['id']]
    # Prepare IO
    dir_in = join_path(BASE_PATH, profile['dir_in'])
    i = 0  # counter to periodically report the progress
    for path in get_all_html_files(dir_in):
        if 'skip_files' in profile:
            basename = os.path.basename(path)
            if basename in profile['skip_files']:
                print("Ignored", path, file=sys.stderr)
                continue
        with open(path) as f:
            soup = parse_to_soup(f.read())
            # Extract the title
            title_entries = soup.select(profile['title'])
            title = title_entries[0].text
            if 'title_garbage_prefix' in profile:
                prefix = profile['title_garbage_prefix']
                length = len(prefix)
                if title.startswith(prefix):
                    title = title[length:]
            if 'title_garbage_suffix' in profile:
                suffix = profile['title_garbage_suffix']
                length = len(suffix)
                if title.endswith(suffix):
                    title = title[:-length]

            # ==============================
            # Text extraction
            # ==============================
            text_entries = soup.select(profile['text'])
            # Remove garbage tags from text
            if 'text_garbage_selectors' in profile:
                for text_entry in text_entries:
                    for gar_select in profile['text_garbage_selectors']:
                        garbage = text_entry.select(gar_select)
                        for element in garbage:
                            element.extract()
            # Clean and collect all entries
            text_entries = [clean(entry, profile['replace_tags']) for entry in text_entries]
            # Join all entries to a single string
            text = '\n'.join(text_entries)
        # Add age information
        year_id = profile['year_info_id_fn'](path)
        if year_id in year_information:
            year_range = year_information[year_id]
        else:
            print('No age information for {} using key "{}"'.format(path, year_id))
            year_range = DEFAULT_YEAR_INFORMATION
        # Write everything
        out_basedir = join_path(BASE_PATH, OUTPUT_DIRECTORY, profile['dir_out'])
        out_path = join_path(out_basedir, profile['filename_fn'](path) + '.json')
        # Create output directories
        os.makedirs(out_basedir, exist_ok=True)
        # The final object has the following form
        #   {
        #       'title': 'TITEL',
        #       'text': 'Content...',
        #       'year_range': {
        #           'start': -300,
        #           'end': 400,
        #       }
        #   }
        final_object = {
            'title':        title.strip(),
            'text':         text.strip(),
            'origin':       profile['id'],
            'year_range':   year_range,
            'unique_name': profile['id'] + '/' + profile['filename_fn'](path),
        }
        # Apply post parsing function if specified
        if 'post_fn' in profile:
            final_object = profile['post_fn'](final_object)

        write_object_to(final_object, out_path)
        
        # periodically report the parsers progress
        if i > 0 and i % 100 == 0:
            print('{} documents parsed'.format(i))
        i += 1
    
    print('finished parsing {} ({} documents)'.format(profile["id"], i))


# The book profiles
BIBEL_PROFILE = {
    'id':                     'Bibel',
    'dir_in':                 '../Bibel',
    'dir_out':                'Bibel',
    'filename_fn':            filename_without_extension,
    'title':                  'title',
    'title_garbage_suffix':   ' – Einheitsübersetzung der Heiligen Schrift (1980) [Quadro-Bibel 5.0]',
    'text':                   'div.v',
    'text_garbage_selectors': ['.fnm', 'span.xa'],
    'year_info_id_fn':        filename_without_number,
    'replace_tags':           [                             # A list of tag replacement operations
        {
            'tag':         'span',                          # The tag name to match against for replacement
            'classes':     ['vn'],                          # A list of classes to match against (all classes must match)
            'new_tag':     'span',                          # The new tag name
            'new_classes': ['number'],         # A list of new classes to add to the new tag
        },
        {
            'tag':         'h2',
            'classes':     [],
            'new_tag':     'h4',
            'new_classes': ['subtitle'],
        },
        {
            'tag':         'h4',
            'classes':     [],
            'new_tag':     'h5',
            'new_classes': ['subsubtitle'],
        },
    ],
}
KORAN_PROFILE = {
    'id':                     'Koran',
    'dir_in':                 '../Koran',
    'dir_out':                'Koran',
    'filename_fn':            filename_without_extension,
    'skip_files':             ['vorwort-von-max-henning.html', 'einleitung-von-max-henning.html'],
    'title':                  'title',
    'title_garbage_suffix':   ' | Koran online Deutsch - kostenlos',
    'text':                   'section.article-content p',
    'text_garbage_selectors': ['img'],
    'year_info_id_fn':        filename_without_extension,
    'replace_tags':           [
        {
            'tag':         'h3',
            'classes':     [],
            'new_tag':     'h4',
            'new_classes': ['subtitle'],
        },
    ],
    'post_fn':                format_koran_vns,
}
TORAH_PROFILE = {
    'id':                     'Torah',
    'dir_in':                 '../Torah',
    'dir_out':                'Torah',
    'filename_fn':            parent_dir_without_extension,
    'title':                  'title',
    'title_garbage_suffix':   ' – talmud.de',
    'title_garbage_prefix':   'Die Torah – ',
    'text':                   '.ToText, .Kapitel',
    'text_garbage_selectors': [],
    'year_info_id_fn':        parent_dir_without_extension,
    'replace_tags':           [
        {
            'tag':         'sup',
            'classes':     ['Vers'],
            'new_tag':     'span',
            'new_classes': ['number'],
        },
        {
            'tag':         'span',
            'classes':     [],
            'new_tag':     'h4',
            'new_classes': ['subtitle'],
        },
    ],
}

if __name__ == '__main__':
    # overwrite output path if we got an argument passed
    if len(sys.argv) > 1:
        OUTPUT_DIRECTORY = sys.argv[1]

    parse(TORAH_PROFILE)
    parse(BIBEL_PROFILE)
    parse(KORAN_PROFILE)
