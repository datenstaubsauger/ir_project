#!/bin/sh

# This script runs solr and once it's up, it will create a collection
# and index the files in a directory

_INDEX_DIR_PRE="/var/solr/religioese_texte_parsed"

# check if base folder for json files exists
if [ -d $_INDEX_DIR_PRE ]; then
    echo "parsed documents folder exists, skipping parser"
else
    echo "running parser"
    python3 /religioese_texte/parser/parser.py $_INDEX_DIR_PRE
fi

# $1 - collection name; $2 - path relative to $_INDEX_DIR_PRE
solr_create_collection () {
	if 	! curl "http://localhost:8983/solr/admin/collections?action=LIST" \
		| grep "$1" ; then
		solr create -c "$1" -s 2 -rf 2
		echo "inserting files for collection $1"
		post -c "$1" "$_INDEX_DIR_PRE/$2"
	fi
}

solr start -c

solr_create_collection bibel Bibel

solr_create_collection koran Koran

solr_create_collection torah Torah

solr_create_collection alle

echo "solr is ready"

# Keep alive, until solr is terminated
while true; do
	sleep 365.26d
done
