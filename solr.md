# solr

## documentation links
* [Quickstart](https://lucene.apache.org/solr/guide/7_7/solr-tutorial.html)
* [Reference](https://lucene.apache.org/solr/guide/7_7/)
* [JavaDoc](https://lucene.apache.org/solr/8_1_0/index.html)
* [Bücher in der Unibib](https://katalog.ub.uni-leipzig.de/Search/Results?lookfor=solr)

## usage

### command line
```
# create a collection
$ /path/to/solr/bin/solr create -c name_of_collection -s number_of-shards -rf number_of_replicas
# index documents
$ /path/to/solr/bin/post -c name_of_collection /path/to/documents
# query using curl
$ curl "http://localhost:8983/solr/#/name_of_collection/query/your_query"
```

### admin interface
* [query builder](http://localhost:8983/solr/#/name_of_collection/query)

### query parameters
* q: word to search for.
     use field:term to only look for the word in a specific field.
     "multi term searches need to be double quoted".
     +two +terms searches for two and terms
     +two -terms searches for two but not terms
* fl: fields to return, combine several with ','; for example fl=*,score
      * returns all default fields
      score returns the ranking of the search result
